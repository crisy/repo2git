# 使用说明：

## Name:
　repo_change_git.sh脚本试用于repo管理的代码转为git单一仓库管控代码

## Description
　repo管理多仓库代码是谷歌推广的代码管控方案，这种方案适用于多模块管理代码，针对定制方案商，往往他们出版本，出软件并不会分模块管控代码，底层上层耦合严重，他们更注重统一管理代码，通过单一git仓库来管控所有的代码，所以提供一种方案，将repo管理的代码转换成单一git仓库管理；
　
## Options	
* --repo2git <git_code>
　　repo代码仓库转为大git仓库

* --format <tag1> <tag2>
　　生成两个tag之间的补丁

* --apply <git_code>
　　将补丁打入指定的git大仓库

* --list <tag1> <tag2>
　　列出两个tag之间的提交

* --checkTag <tag>
　　校验repo所有仓库某个tag是否存在

* --dos2unix <path>
　　指定类型文件转换成unix格式

* --init
　　第一次转换时初始化git仓库

* --modify
　　修改repo生成的补丁,适应打入git



