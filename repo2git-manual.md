repo仓库初始化转化手册：

制作基础Git仓库：
1.准备两套repo仓库，一套作为基础大git仓库 <code_git>，一套作为待转化repo仓库 <code_repo>
2.把code_git仓库的代码回退到初始提交状态，repo_change_git.sh --init
3.转换完了之后大致进去三四个repo小仓库文件夹里面，git log 看下是否已经会已经回退正确
4.如果回退正确，删除code_git 仓库里面的.git 和 .repo , 操作步骤：在code_git的android目录,rm -rf .repo ; find . -iname .git -exec rm -rf {} ';'
5.此时这个代码初始化就完成了，git init ； git add -A ；git commit -m “common：first-version”
6.设置初始化tag标签，我们当前时间的前一天作为标签值，比如今天是0811，这里就用0810；操作步骤：在code_git的android目录,git tag -a ali_base_20160810 -m 20160810 
7.由于部分代码带着windows的格式，所以需要修改git仓库的编码格式，repo_change_git.sh --dos2unix <code_git>路径
8.执行完后再做一次add commit提交。作为基础的git仓库就算准备完毕了。


repo转git：
1.给repo仓库设置初始化标签节点：在<code_repo>仓库设置初始化节点，执行repo_change_git.sh  --test set_tag_of_commit 20160810(这里的时间值要匹配上面设置的时间值)
2.生成patch准备合入<code_git>，repo_change_git.sh --repo2git <code_git>路径
3.后续脚本自己运行合入patch，如果出现冲突，根据提示，解决冲突，然后执行git am --reseved 
4.然后将已经错误的patch打上fail的标签，mv failed.patch failed.patch-fail
5.然后继续执行repo_change_git.sh --apply <code_git>路径,继续合入补丁
6.如果还出现错误，步骤参考3-4-5，如此反复
7.补丁打完后，根据打印提示，到<code_git>代码下，将最新的tag标签打上，这个标签和<code_repo>代码的标签一致，作为两边代码已经一致的标志位


后续更新：
1.<code_repo>执行repo_change_git.sh --sync更新到最新代码，脚本会自动打上当天为最新的标签标志位
2.生成补丁打入<git-code>仓库，repo_change_git.sh --repo2git <code_git>路径即可
3.如果出现冲突，使用repo转git的3-4-5步骤解决，方法同上



附录：
 两种解决冲突的方法:
 1.解决冲突1
   强制合入git apply PATCH --reject
   根据生成的.rej文件合入冲突地方.编辑冲突文件，解决冲突
   译注：根据.rej文件手动解决所有冲突）
   git add FIXED_FILES
   git am --resolved
   然后mv xxx.patch xxx.patch-fail
   然后apply继续打补丁
 
 2.解决冲突2
   出现冲突后,用git apply --check检查下哪些文件导致无法打入patch,然后找出解决方案
   一般都是由于文件类型问题,check后
   然后fromdos这些文件
   然后git add -A
   然后am --resolved
   然后mv xxx.patch xxx.patch-fail
   然后apply继续打补丁
   git apply 0001-add-line.patch --check
   fromdos xxxfile
   git apply 0001-add-line.patch
   git add -A
   git am --resolved
   在解决完冲突以后,比如用git add来让git知道你已经解决完冲突了.
 

 
 

